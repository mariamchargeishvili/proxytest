jQuery(document).ready(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });




    $(document).on('click','.delete-url',function (event) {
        console.log(this.value)

        var id=$(this).val()
        $.ajax({
            type: 'delete',
            url: '/admin/url/'+id,
            success: function(data) {
                $('.row'+id).remove();
            }
        })
    })


    $(document).on('click','.delete-email',function (event) {
        console.log(this.value)

        var id=$(this).val()
        $.ajax({
            type: 'delete',
            url: '/admin/email/'+id,
            success: function(data) {
                $('.row'+id).remove();
            }
        })
    })

    $(document).on('click','.delete-user',function (event) {
        console.log(this.value)

        var id=$(this).val()
        $.ajax({
            type: 'delete',
            url: '/admin/user/'+id,
            success: function(data) {
                $('.row'+id).remove();
            }
        })
    })








});