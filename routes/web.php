<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


//Route::match(['get', 'post'], 'register', function(){
//    return redirect('/');
//});

Route::post('/home', 'TokenController@checkToken');

Route::get('/getcode', 'TokenController@getcode');



Route::group(['middleware'=>['auth','admin'],'prefix'=>'/admin'],function() {
    Route::get('/', 'AdminController@index');

    Route::get('users','AdminController@users');
    Route::delete('user/{id}','AdminController@deleteUser');



    Route::get('/all_log', function () {
        return view('admin.logs');
    });
    Route::get('/user/{id}', 'AdminController@logsOnUser');


    Route::get('/sites', 'AdminController@sitelist');
    Route::post('/sites/create', 'AdminController@createUrl');
    Route::delete('/url/{id}','AdminController@deleteUrl');


    Route::get('/emails', 'AdminController@emails');
    Route::post('/email/create', 'AdminController@createEmail');
    Route::delete('/email/{id}','AdminController@deleteEmail');


    Route::get('/register', function () {
        return view('admin.register');
        return view('admin.register');
    });

    Route::post('/saveuser','AdminController@saveuser');


    Route::get('/search', function () {
        return view('admin.search');
    });

    Route::post('/search','FileController@search');
    Route::get('/file/{name}','FileController@getFileContent');

    Route::get('/filelogs','DatatablesController@index');

    Route::post('/datatables/data','DatatablesController@getData');
    Route::post('/datatables/logs','DatatablesController@getLogs');


});
Route::get('test','FileController@index');
Route::get('delete','FileController@deleteRecords');







