<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Filelog extends Model
{
    protected $fillable = [
        'address',
        'time'
    ];

    protected $table="filelogs";
}
