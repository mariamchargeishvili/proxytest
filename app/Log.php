<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{

    protected $table='logs';

    protected $fillable = [

        'url',
        'ip',
        'comp_name',
        'user_name',
        'computer_id'

    ];

    public function computer(){

        return $this->belongsTo('App\Computer');
    }
}
