<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Computer extends Model
{
    protected $table="computers";

    protected $fillable=[
        'name'
    ];

    public function logs(){

        return $this->hasMany('App\Log');

    }


}
