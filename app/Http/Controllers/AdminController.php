<?php

namespace App\Http\Controllers;

use App\Code;
use App\Computer;
use App\Email;
use App\Log;
use App\Site;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Cache;
use Illuminate\Support\Facades\Hash;


class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }




    public function index(){


        if(Cache::get('authCode') != null){

            $authCode =Cache::get('authCode');

        }else{

            $authCode = mt_rand(100000, 999999);
            Cache::put('authCode', $authCode, 2000);

        }


        $type = (Auth::user()->type);
        switch ($type){
            case  1:
                return view('admin.administrator',compact('authCode'));
                break;
            case  2:
                $comps = Computer::all();
                return view('admin.users',compact('comps'));
                break;
            case  3:
                $comps = Computer::all();
                return view('admin.users',compact('comps'));
                break;

        }

    }








    public  function logsOnUser( $id){

        $logs = Log::where('computer_id',$id)

            ->get()
            ->groupBy(function($date) {
                return Carbon::parse($date->created_at)->format('d-m-Y'); // grouping by years
            });


        return view('admin.dayWithLogs',compact('logs'));
    }










    /** URL LIST PAGE WITH ACTIONS
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function sitelist(){

        $sites = Site::all();

        return view('admin.sites',compact('sites'));
    }



    public function createUrl(Request $request){

        $request = $request->all();
        Site::create(['url' => $request['url']]);

        return redirect()->action('AdminController@sitelist');

    }



    public function deleteUrl($id){

        Site::destroy($id);

        return response()->json([],200);
    }









    /** EMAIL PAGE WITH ACTIONS
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function emails(){

        $sites = Email::all();

        return view('admin.emails',compact('sites'));
    }



    public function createEmail(Request $request){

        $request = $request->all();

        Email::create(['email' => $request['email']]);

        return redirect()->action('AdminController@emails');

    }



    public function deleteEmail($id){

        Email::destroy($id);

        return response()->json([],200);
    }






    /** Registered user Controll
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function saveuser(Request $request){

        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'type' => 'required',

        ]);

        $data = $request->all();


        User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'type' => $data['type'],
            'password' => Hash::make($data['password']),
        ]);

        return redirect()->action('AdminController@users');

    }



    public function users(){

        $users = (User::all());

        return view('admin.userList',compact('users'));
    }


    public function deleteUser($id){

        User::destroy($id);

        return response()->json([],200);
    }



}
