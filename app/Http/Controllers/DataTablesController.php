<?php

namespace App\Http\Controllers;

use App\Computer;
use App\Filelog;
use App\Log;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;


class DataTablesController extends Controller
{


    public function index(){

        return view('admin.filelog');
    }

    public function returnJSON($request,$model){

        $searchWord = ($request['search']['value']);
        $start = $request['start'];
        $length = $request['length'];
        $totalData = Filelog::count();
        $dir = $request->input('order.0.dir');
        $filteredData = $totalData;


        if(!$searchWord){
            $data=$model::offset($start)
                ->limit($length)
                ->get();
        }else{
            $data=$model::where('address','LIKE',"%{$searchWord}%")
                ->orWhere('time','LIKE',"%{$searchWord}%")
                ->offset($start)
                ->limit($length)
                ->get();

            $filteredData = Filelog::where('address','LIKE',"%{$searchWord}%")
                ->orWhere('time','LIKE',"%{$searchWord}%")->count();
        }



        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($filteredData),
            "data"            => $data
        );

        return $json_data;

    }




    public function getData(Request $request){


        $model = new Filelog();
        $json_data = $this->returnJSON($request,$model);
        return json_encode($json_data);



    }


    public function getLogs( Request $request){


        $model = new Log();
        $json_data = $this->returnJSON($request,$model);
        $arr = [];
        foreach ($json_data['data'] as $data){
            $user =(Computer::find($data['computer_id']));
            $data['computer_id'] = ($user->name);
            array_push($arr,$data);
        }
        return json_encode($json_data);

    }
}
