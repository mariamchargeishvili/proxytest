<?php

namespace App\Http\Controllers;

use App\Filelog;
use App\Log;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Email;

class FileController extends Controller
{

    /**
     *
     */
    public function index(){


        $conn_id = ftp_connect('192.168.11.117');
        $login_result = ftp_login($conn_id, 'dev', 'dev123');
        $contents = ftp_nlist($conn_id, "/log");



        foreach ($contents as $content){

                $times = ftp_nlist($conn_id, $content);
                if($times){

                    $lastTime = $times[sizeof($times)-1];
                    $arr = explode('/',$lastTime);
                    $time = $arr[sizeof($arr)-1];
                    $time=(substr($time,0,10));
                    $carbonInstance = Carbon::parse($time);
                    $today = Carbon::today();
                    if(!$carbonInstance->eq($today)){

                        Filelog::create(['address' => $content , 'time' => $today]);
                        $emails = Email::all();

                        $body = 'User '.$content.' missed log file Time: '.$today;

                        $headers = '';
                        $headers .= "Reply-To: It Manager <itmanager@nextbrand.org>\r\n";
                        $headers .= "Return-Path: It Manager <itmanager@nextbrand.org>\r\n";
                        $headers .= "From: It Manager <itmanager@nextbrand.org>\r\n";
                        $headers .= "Organization: Next Brand\r\n";
                        $headers .= "MIME-Version: 1.0\r\n";
                        $headers .= "Content-type: text/plain; charset=iso-8859-1\r\n";
                        $headers .= "X-Priority: 3\r\n";
                        $headers .= "X-Mailer: PHP". phpversion() ."\r\n" ;

                        foreach ($emails as $email){

                            $obj = mail($email->email,'missed log file on user',$body,$headers);
                        }


                    }

                }

        }


    }



    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(Request $request){


        $request->validate([
              'search' => 'required'
        ]);

        $start = $request->all()['start'];
        $end = $request->all()['end'] == null ? Carbon::now()->format('Y-m-d') : $request->all()['end'];
        $start = $request->all()['start'] == null ? Carbon::now()->addYear(-1)->format('Y-m-d') : $request->all()['start'];

        $word = $request->all()["search"];

        $conn_id = ftp_connect('192.168.11.117');
        $login_result = ftp_login($conn_id, 'dev', 'dev123');
        $contents = ftp_nlist($conn_id, "/log");


        $searched = true;
        $wordMatchFiles = [];
        foreach ($contents as $content){

            $items = ftp_nlist($conn_id, $content);

            foreach ($items as $item){

                $arr = explode('/',$item);
                $time = substr(array_pop($arr),0,10);


                ob_start();
                $result = ftp_get($conn_id, "php://output", $item."/keystrokes.html", FTP_BINARY);
                $data = ob_get_contents();
                $state = (stristr ($data, $word)) ? true : false;

                ob_end_clean();

                if($state){
                    if(array_search($content,$wordMatchFiles,true) === false){
                        array_push($wordMatchFiles,$item."/keystrokes.html");
                    }
                }
            }
        }


        return view('admin.search',compact('wordMatchFiles','searched'));



    }




    /**
     * @param $name
     */
    public function getFileContent($name){


        $conn_id = ftp_connect('192.168.11.117');

        $login_result = ftp_login($conn_id, 'dev', 'dev123');
        $contents = ftp_nlist($conn_id, "/log");
        $fileName = str_replace('_','/',$name);


        ob_start();
        $result = ftp_get($conn_id, "php://output", $fileName , FTP_BINARY);
        $data = ob_get_contents();
        ob_end_clean();

        echo ($data);


    }




    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function filelog(){

        $items = Filelog::all();

        return view('admin.filelog',compact('items'));
    }



    public function deleteRecords(){

        Log::where('created_at','<',Carbon::now()->addMonth(-2))->delete();
        Filelog::where('created_at','<',Carbon::now()->addMonth(-2))->delete();

    }
}
