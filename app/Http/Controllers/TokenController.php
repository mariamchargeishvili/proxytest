<?php

namespace App\Http\Controllers;

use App\Code;
use App\Computer;
use App\Email;
use App\Log;
use App\Site;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Cache;

class TokenController extends Controller
{


    public function checkToken(Request $request){

        $request->validate([
            'token' =>'required'
        ]);

        $token = ($request->all()['token']);
        if(Cache::get('authCode') == $token){
             return view('layouts.proxy',compact('content'));
        }else{
            return redirect()->back();
        }



    }

    public function getcode(){

       return Cache::get('authCode');
    }


    public function insertLogs(Request $request){


        $req = ($request->all());
        $comp = Computer::where('name',$req['user_name'])->first();
        if(!$comp){
            $comp = Computer::create(['name' => $req['user_name']]);
        }
        $url = ($req['url']);

        Log::create(['ip' => $req['ip'],'url' => $req['url'],'comp_name' => $req['comp_name'], 'computer_id' => $comp->id]);




        $url = Site::where('url',parse_url($req['url'], PHP_URL_HOST))->first();




        if($url){
            $emails = Email::all();

            $subject = 'User '.$comp->name.' Accessed To Restrected Resource: '.$req['url'];

            $headers = '';
            $headers .= "Reply-To: It Manager <itmanager@nextbrand.org>\r\n";
            $headers .= "Return-Path: It Manager <itmanager@nextbrand.org>\r\n";
            $headers .= "From: It Manager <itmanager@nextbrand.org>\r\n";
            $headers .= "Organization: Next Brand\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/plain; charset=iso-8859-1\r\n";
            $headers .= "X-Priority: 3\r\n";
            $headers .= "X-Mailer: PHP". phpversion() ."\r\n" ;

            foreach ($emails as $email){
                mail($email->email,'access to restrected websites',$subject,$headers);


            }
        }



    }
}
