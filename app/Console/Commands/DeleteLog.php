<?php

namespace App\Console\Commands;

use App\Filelog;
use App\Log;
use Carbon\Carbon;
use Illuminate\Console\Command;

class DeleteLog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::where('created_at','<', Carbon::now()->addMonth(-2))->destroy();
        Filelog::where('created_at','<',Carbon::now()->addMonth(-2))->destroy();
    }
}
