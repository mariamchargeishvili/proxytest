@extends('admin.app')
@section('content')

<div class="card">
    <div class="card-body">
        <h4 class="card-title">Logs Table</h4>

        @foreach($logs as $key => $values)
            <h5 style="fornt-waight:bold; margin-top: 20px">{{$key}}</h5>
                <div style="margin-left:20px">
                    @foreach($values as $value)
                        <p>
                            {{$value->url}}
                        </p>
                    @endforeach

                </div>
        @endforeach

    </div>
</div>

@endsection