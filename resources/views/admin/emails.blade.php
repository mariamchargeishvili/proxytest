@extends('admin.app')
@section('content')

<div class="card">
    <div class="card-body">
        <h4 class="card-title">Users Table</h4>
        <form action="{{url('/admin/email/create')}}" method="post">
            @csrf

            <input type="email" name="email">
            <button type="submit" class="btn btn-primary">Add</button>

        </form>


        <div class="table-responsive">
            <table id="data-table" class="table table-bordered">
                <thead class="thead-default">
                <tr>
                    <th>Url</th>
                    <th>Actions</th>

                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>Url</th>
                    <th>Actions</th>
                </tr>
                </tfoot>
                <tbody>

                    @foreach($sites as $item)
                        <tr class="row{{$item['id']}}">
                            <td>{{$item->email}}</td>
                            <td>
                                <span class="oi oi-delete"></span>
                                <button type="submit" class="delete-email" value={{$item['id']}}>delete</button>
                            </td>
                        </tr>
                    @endforeach


                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection