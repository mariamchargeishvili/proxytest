@extends('admin.app')
@section('content')

<div class="card">
    <div class="card-body">
        <h4 class="card-title">Users Table</h4>
        <form action="{{url('/admin/sites/create')}}" method="post">
            @csrf

            <input type="text" name="url">
            <button type="submit" class="btn btn-primary">Add</button>

        </form>


        <div class="table-responsive">
            <table id="data-table" class="table table-bordered">
                <thead class="thead-default">
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Type</th>
                    <th>Actions</th>

                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Type</th>
                    <th>Actions</th>
                </tr>
                </tfoot>
                <tbody>

                    @foreach($users as $item)
                        <tr class="row{{$item['id']}}">
                            <td>{{$item->name}}</td>
                            <td>{{$item->email}}</td>
                            @if($item->type == 1)
                                <td>Manager</td>
                            @elseif($item->type == 2)
                                <td>Administrator</td>
                            @else
                                <td>Access to keylogger</td>
                            @endif
                            <td>
                                <span class="oi oi-delete"></span>
                                <button type="submit" class="delete-user" value={{$item['id']}}>delete</button>
                            </td>
                        </tr>
                    @endforeach


                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection