@extends('admin.app')
@section('content')

<div class="card">
    <div class="card-body">
        <h4 class="card-title">Users Table</h4>

        <div class="table-responsive">
            <table id="data-table" class="table table-bordered">
                <thead class="thead-default">
                <tr>
                    <th>Name</th>

                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>Name</th>
                </tr>
                </tfoot>
                <tbody>

                @foreach($comps as $comp)
                    <tr>
                        <td><a href="{{url('admin/user/'.$comp->id)}}">{{$comp->name}}</a></td>
                    </tr>
                @endforeach


                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection