@extends('admin.app')
@section('content')

<div class="card">
    <div class="card-body">
         <div class="table-responsive">
            <table id="logs" class="table table-bordered" >
                <thead class="thead-default">
                    <tr>
                        <th>address</th>
                        <th>time</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>address</th>
                        <th>time</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

@endsection


@section('scripts')
<script>

$(function () {
    $(document).ready(function() {
        if ( $.fn.dataTable.isDataTable( '#logs' ) ) {
            table = $('#logs').DataTable();
        }
        else {
            table = $('#logs').DataTable( {

                "processing": true,
                "serverSide": true,
                // "paging" : true,
                // "deferLoading": 20,

                ajax:{
                    url:'{!! "/admin/datatables/data" !!}',
                    type:'POST',
                    data: function (d) {
                        // console.log(d)

                    }
                },
                columns: [

                    { "data": 'address', "name":"address" },
                    { "data": 'time'  , "name":"time" }

                ],

            } );
        }

    });
})


</script>
@endsection
