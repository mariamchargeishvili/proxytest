@extends('admin.app')
@section('content')

<div class="card">
    <div class="card-body">
        <h4 class="card-title">Logs Table</h4>

        <div class="table-responsive">
            <table id="logs-table" class="table table-bordered">
                <thead class="thead-default">
                <tr>
                    <th>url</th>
                    <th>created at</th>
                    <th>computer_id</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>url</th>
                    <th>created at</th>
                    <th>computer_id</th>

                </tr>
                </tfoot>

            </table>
        </div>
    </div>
</div>

@endsection



@section('scripts')
    <script>

        $(function () {
            $(document).ready(function() {
                if ( $.fn.dataTable.isDataTable( '#logs-table' ) ) {
                    table = $('#logs-table').DataTable();
                }
                else {

                    table = $('#logs-table').DataTable( {

                        "processing": true,
                        "serverSide": true,

                        ajax:{
                            url:'{!! "/admin/datatables/logs" !!}',
                            type:'POST',
                            data: function (d) {
                            }
                        },
                        columns: [

                            { "data": 'url', "name":"url" },
                            { "data": 'created_at'  , "name":"created_at" },
                            { "data": 'computer_id'  , "name":"computer_id" }


                        ],

                    } );
                }

            });
        })


    </script>
@endsection