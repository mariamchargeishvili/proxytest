<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Vendor styles -->
    <link rel="stylesheet" href="{{url('vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css')}}">
    <link rel="stylesheet" href="{{url('vendors/bower_components/animate.css/animate.min.css')}}">
    <link rel="stylesheet" href="{{url('vendors/bower_components/jquery.scrollbar/jquery.scrollbar.css')}}">

    <!-- App styles -->
    <link rel="stylesheet" href="{{url('adminStyles/css/app.min.css')}}">


    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/latest/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

</head>

<body data-ma-theme="green">
<main class="main">
    <div class="page-loader">
        <div class="page-loader__spinner">
            <svg viewBox="25 25 50 50">
                <circle cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>

    <header class="header">
        <div class="navigation-trigger hidden-xl-up" data-ma-action="aside-open" data-ma-target=".sidebar">
            <div class="navigation-trigger__inner">
                <i class="navigation-trigger__line"></i>
                <i class="navigation-trigger__line"></i>
                <i class="navigation-trigger__line"></i>
            </div>
        </div>

        <div class="header__logo hidden-sm-down">
            <h1><a href="#">Admin Panel</a></h1>
        </div>

        <form class="search">
            <div class="search__inner">
                <input type="text" class="search__text" placeholder="Search for people, files, documents...">
                <i class="zmdi zmdi-search search__helper" data-ma-action="search-close"></i>
            </div>
        </form>

        <ul class="top-nav">
            <li class="hidden-xl-up"><a href="" data-ma-action="search-open"><i class="zmdi zmdi-search"></i></a></li>



            {{--<li class="dropdown hidden-xs-down">--}}
                {{--<a href="" data-toggle="dropdown"><i class="zmdi zmdi-apps"></i></a>--}}

                {{--<div class="dropdown-menu dropdown-menu-right dropdown-menu--block" role="menu">--}}
                    {{--<div class="row app-shortcuts">--}}
                        {{--<a class="col-4 app-shortcuts__item" href="">--}}
                            {{--<i class="zmdi zmdi-calendar"></i>--}}
                            {{--<small class="">Calendar</small>--}}
                            {{--<span class="app-shortcuts__helper bg-red"></span>--}}
                        {{--</a>--}}
                        {{--<a class="col-4 app-shortcuts__item" href="">--}}
                            {{--<i class="zmdi zmdi-file-text"></i>--}}
                            {{--<small class="">Files</small>--}}
                            {{--<span class="app-shortcuts__helper bg-blue"></span>--}}
                        {{--</a>--}}
                        {{--<a class="col-4 app-shortcuts__item" href="">--}}
                            {{--<i class="zmdi zmdi-email"></i>--}}
                            {{--<small class="">Email</small>--}}
                            {{--<span class="app-shortcuts__helper bg-teal"></span>--}}
                        {{--</a>--}}
                        {{--<a class="col-4 app-shortcuts__item" href="">--}}
                            {{--<i class="zmdi zmdi-trending-up"></i>--}}
                            {{--<small class="">Reports</small>--}}
                            {{--<span class="app-shortcuts__helper bg-blue-grey"></span>--}}
                        {{--</a>--}}
                        {{--<a class="col-4 app-shortcuts__item" href="">--}}
                            {{--<i class="zmdi zmdi-view-headline"></i>--}}
                            {{--<small class="">News</small>--}}
                            {{--<span class="app-shortcuts__helper bg-orange"></span>--}}
                        {{--</a>--}}
                        {{--<a class="col-4 app-shortcuts__item" href="">--}}
                            {{--<i class="zmdi zmdi-image"></i>--}}
                            {{--<small class="">Gallery</small>--}}
                            {{--<span class="app-shortcuts__helper bg-light-green"></span>--}}
                        {{--</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</li>--}}

            <li class="dropdown hidden-xs-down">
                <a href="" data-toggle="dropdown"><i class="zmdi zmdi-more-vert"></i></a>

                <div class="dropdown-menu dropdown-menu-right">
                    <div class="dropdown-item theme-switch">
                        Theme Switch

                        <div class="btn-group btn-group-toggle btn-group--colors" data-toggle="buttons">
                            <label class="btn bg-green active"><input type="radio" value="green" autocomplete="off" checked></label>
                            <label class="btn bg-blue"><input type="radio" value="blue" autocomplete="off"></label>
                            <label class="btn bg-red"><input type="radio" value="red" autocomplete="off"></label>
                            <label class="btn bg-orange"><input type="radio" value="orange" autocomplete="off"></label>
                            <label class="btn bg-teal"><input type="radio" value="teal" autocomplete="off"></label>

                            <div class="clearfix mt-2"></div>

                            <label class="btn bg-cyan"><input type="radio" value="cyan" autocomplete="off"></label>
                            <label class="btn bg-blue-grey"><input type="radio" value="blue-grey" autocomplete="off"></label>
                            <label class="btn bg-purple"><input type="radio" value="purple" autocomplete="off"></label>
                            <label class="btn bg-indigo"><input type="radio" value="indigo" autocomplete="off"></label>
                            <label class="btn bg-brown"><input type="radio" value="brown" autocomplete="off"></label>
                        </div>
                    </div>
                    <a href="" class="dropdown-item">Fullscreen</a>
                    <a href="" class="dropdown-item">Clear Local Storage</a>
                </div>
            </li>

            <li class="hidden-xs-down">
                <a href="" data-ma-action="aside-open" data-ma-target=".chat" class="top-nav__notify">
                    <i class="zmdi zmdi-comment-alt-text"></i>
                </a>
            </li>
        </ul>
    </header>

    <aside class="sidebar">
        <div class="scrollbar-inner">
            <div class="user">
                <div class="user__info" data-toggle="dropdown">
                    <img class="user__img" src="demo/img/profile-pics/8.jpg" alt="">
                    <div>
                        <div class="user__name">{{\Illuminate\Support\Facades\Auth::user()['name']}}</div>
                        <div class="user__email">{{\Illuminate\Support\Facades\Auth::user()['email']}}</div>
                    </div>
                </div>

                <div class="dropdown-menu">
                    <a class="dropdown-item" ></a>

                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">
                        Logout
                    </a>
                    <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>

            <ul class="navigation">
                <li><a href="{{url('/admin')}}"><i class="zmdi zmdi-view-list"></i> Users</a></li>
                <li><a href="{{url('admin/all_log')}}"><i class="zmdi zmdi-view-list"></i> All log</a></li>
                <li><a href="{{url('/admin/sites')}}"><i class="zmdi zmdi-view-list"></i> Blocked Sites</a></li>
                <li><a href="{{url('/admin/emails')}}"><i class="zmdi zmdi-view-list"></i> Email Lists</a></li>
                <li><a href="{{url('/admin/register')}}"><i class="zmdi zmdi-view-list"></i> Register User</a></li>
                <li><a href="{{url('/admin/users')}}"><i class="zmdi zmdi-view-list"></i> Users List</a></li>

                <li><a href="{{url('/admin/search')}}"><i class="zmdi zmdi-view-list"></i> Search Word in File</a></li>
                <li><a href="{{url('/admin/filelogs')}}"><i class="zmdi zmdi-view-list"></i> Unlogged files list</a></li>



            </ul>
        </div>
    </aside>

    <section class="content">
        <header class="content__title">

            <div class="actions">
                <a href="" class="actions__item zmdi zmdi-trending-up"></a>
                <a href="" class="actions__item zmdi zmdi-check-all"></a>

                <div class="dropdown actions__item">
                    <i data-toggle="dropdown" class="zmdi zmdi-more-vert"></i>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="" class="dropdown-item">Refresh</a>
                        <a href="" class="dropdown-item">Manage Widgets</a>
                        <a href="" class="dropdown-item">Settings</a>
                    </div>
                </div>
            </div>
        </header>


        @yield('content')

        <footer class="footer hidden-xs-down">
            <p>© Material Admin Responsive. All rights reserved.</p>

            {{--<ul class="nav footer__nav">--}}
                {{--<a class="nav-link" href="">Homepage</a>--}}

                {{--<a class="nav-link" href="">Company</a>--}}

                {{--<a class="nav-link" href="">Support</a>--}}

                {{--<a class="nav-link" href="">News</a>--}}

                {{--<a class="nav-link" href="">Contacts</a>--}}
            {{--</ul>--}}
        </footer>
    </section>
</main>

<!-- Older IE warning message -->
<!--[if IE]>
<div class="ie-warning">
    <h1>Warning!!</h1>
    <p>You are using an outdated version of Internet Explorer, please upgrade to any of the following web browsers to access this website.</p>

    <div class="ie-warning__downloads">
        <a href="http://www.google.com/chrome">
            <img src="img/browsers/chrome.png" alt="">
        </a>

        <a href="https://www.mozilla.org/en-US/firefox/new">
            <img src="img/browsers/firefox.png" alt="">
        </a>

        <a href="http://www.opera.com">
            <img src="img/browsers/opera.png" alt="">
        </a>

        <a href="https://support.apple.com/downloads/safari">
            <img src="img/browsers/safari.png" alt="">
        </a>

        <a href="https://www.microsoft.com/en-us/windows/microsoft-edge">
            <img src="img/browsers/edge.png" alt="">
        </a>

        <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
            <img src="img/browsers/ie.png" alt="">
        </a>
    </div>
    <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->

<!-- Javascript -->
<!-- Vendors -->



<script src="{{url('vendors/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{url('vendors/bower_components/popper.js/dist/umd/popper.min.js')}}"></script>
<script src="{{url('vendors/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{url('vendors/bower_components/jquery.scrollbar/jquery.scrollbar.min.js')}}"></script>
<script src="{{url('vendors/bower_components/jquery-scrollLock/jquery-scrollLock.min.js')}}"></script>

<!-- Vendors: Data tables -->
<script src="{{url('vendors/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{url('vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{url('vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{url('vendors/bower_components/jszip/dist/jszip.min.js')}}"></script>
<script src="{{url('vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>

<!-- App functions and actions -->
<script src="{{url('adminStyles/js/app.min.js')}}"></script>

<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>


<script src="{{url('js/admin.js')}}"></script>

@yield('scripts')


</body>
</html>