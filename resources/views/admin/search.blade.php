@extends('admin.app')
@section('content')

<div class="card">
    <div class="card-body">
        <h4 class="card-title">Search Word in Files</h4>
        <form action="{{url('/admin/search')}}" method="post">
            @csrf

            <input type="text" name="search">

            <input type="date" name="start">
            <input type="date" name="end">
            <button type="submit" class="btn btn-primary">go</button>


        </form>


        @if(isset($wordMatchFiles) && $wordMatchFiles != [])

                <div class="table-responsive">
                    <table id="data-table" class="table table-bordered">
                        <thead class="thead-default">
                        <tr>
                            <th>File Address</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>File Address</th>
                        </tr>
                        </tfoot>
                        <tbody>
                            @foreach($wordMatchFiles as $item)
                                <tr>
                                    <td><a href="{{url('admin/file/'.str_replace('/','_',$item))}}">{{$item}}</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            @elseif(isset($searched) && $searched)

                 <div style="margin-top: 30px" class="alert alert-danger alert-dismissible fade show" role="alert">
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                         <span aria-hidden="true">×</span>
                     </button>
                     Nothing was found! Change the word an try again.
                 </div>
        @endif

</div>

@endsection

@section('scripts')




@endsection