<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url');
            $table->string('ip');
            $table->string('comp_name');
            $table->integer('computer_id')->unsigned()->nullable();

            $table->timestamps();



        });
        Schema::table('logs', function($table) {
            $table->foreign('computer_id')->references('id')->on('computers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs');
    }
}
